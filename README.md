# The-Muppets Mirror Manifest

Usage: `repo init -u https://gitlab.com/The-Muppets/manifest -b mirror --mirror`

Once the mirror is synced you can then run `repo init -u /path/to/The-Muppets/mirror/manifest.git -b $BRANCHNAME` and sync normally.

If you want to sync the source quickly but want it to be up-to-date without syncing the mirror every time, then run `repo init -u http://www.gitlab.com/The-Muppets/manifest -b $BRANCHNAME --reference=/path/to/The-Muppets/mirror/`. This will init the new repo and fetch all the (available) data from the mirror, but will fallback to GitLab if something is missing in the mirror.